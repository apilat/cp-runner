use crate::language::{CompiledLanguage, InterpretedLanguage, Language};
use crate::test::{Outcome, Test};
use anyhow::{anyhow, Context, Error};
use std::convert::TryInto;
use std::ffi::OsStr;
use std::fmt::Write as FmtWrite;
use std::fs::OpenOptions;
use std::io::{ErrorKind, Write};
use std::path::PathBuf;
use std::process::{Command, Stdio};

#[derive(Debug)]
pub enum Program {
    Compiled(CompiledProgram),
    Interpreted(InterpretedProgram),
}

#[derive(Debug)]
pub struct CompiledProgram {
    source: PathBuf,
    target: PathBuf,
    language: CompiledLanguage,
}

#[derive(Debug)]
pub struct InterpretedProgram {
    executable: PathBuf,
    language: InterpretedLanguage,
}

impl Program {
    pub fn create(path: PathBuf, mut template: PathBuf) -> Result<Self, Error> {
        if path.file_name().is_none() {
            return Err(anyhow!("no filename"));
        }

        if let Some(lang) = path
            .extension()
            .and_then(OsStr::to_str)
            .and_then(Language::by_extension)
        {
            if template.extension().is_none() {
                template.set_extension(lang.extension());
            }
            let content = std::fs::read_to_string(&template).context("template read")?;

            let mut file = match OpenOptions::new().write(true).create_new(true).open(&path) {
                Ok(x) => x,
                Err(err) => match err.kind() {
                    ErrorKind::AlreadyExists => return Err(anyhow!("file already exists")),
                    _ => return Err(anyhow!(err).context("template open")),
                },
            };

            file.write_all(content.as_bytes())
                .context("template write")?;

            Ok(Self::existing(path, lang))
        } else {
            Err(anyhow!("unknown extension"))
        }
    }

    fn existing(path: PathBuf, language: Language) -> Self {
        assert!(path.extension().and_then(OsStr::to_str) == Some(language.extension()));
        assert!(path.is_file());
        match language {
            Language::Interpreted(language) => Program::Interpreted(InterpretedProgram {
                executable: path,
                language,
            }),
            Language::Compiled(language) => Program::Compiled(CompiledProgram {
                target: {
                    let mut target = PathBuf::new();
                    if path.is_relative() {
                        target.push("./");
                    }
                    target.push(&path);
                    target.set_extension("");
                    target
                },
                source: path,
                language,
            }),
        }
    }

    /// Attempts to find an existing program with the given path and an extension mathcing a known language.
    pub fn find_by_path(mut path: PathBuf) -> Result<Self, Error> {
        if path.file_name().is_none() {
            return Err(anyhow!("no filename"));
        }

        if path.is_file() {
            // If file has an extension, it must be valid. Otherwise, just continue to checking all extensions.
            if let Some(extension) = path.extension().and_then(OsStr::to_str) {
                return match Language::by_extension(extension) {
                    None => Err(anyhow!("unknown extension {}", extension)),
                    Some(lang) => Ok(Self::existing(path, lang)),
                };
            }
        }

        let mut possible_files = Vec::new();
        for lang in Language::iter() {
            path.set_extension(lang.extension());
            if path.is_file() {
                possible_files.push((path.clone(), lang));
            }
        }

        if possible_files.is_empty() {
            Err(anyhow!("file not found"))
        } else if possible_files.len() == 1 {
            let [(path, lang)]: [(PathBuf, Language); 1] = possible_files.try_into().unwrap();
            Ok(Self::existing(path, lang))
        } else {
            Err(anyhow!(
                "ambiguous name: could be {}",
                possible_files
                    .iter()
                    .fold((String::new(), true), |(mut acc, first), (path, _lang)| {
                        if !first {
                            acc.push_str(", ");
                        }
                        write!(&mut acc, "{}", path.display()).unwrap();
                        (acc, false)
                    })
                    .0
            ))
        }
    }

    /// Prepares a program for execution.
    ///
    /// For compiled languages, this will re-compile the source if it is newer than the existing executable or `force_recompile` is specified.
    /// For interpreted languages, this is a no-op.
    pub fn prepare(&self, debug: bool, force_recompile: bool) -> Result<(), Error> {
        match self {
            Program::Compiled(prog) => {
                if force_recompile {
                    prog.recompile(debug)
                } else {
                    prog.recompile_if_old(debug)
                }
            }
            Program::Interpreted(_) => Ok(()),
        }
    }

    fn get_execute_cmd(&self) -> Command {
        match self {
            Program::Compiled(prog) => Command::new(&prog.target),
            Program::Interpreted(prog) => {
                let mut cmd = Command::new(prog.language.interpreter());
                cmd.arg(&prog.executable);
                cmd
            }
        }
    }

    /// Executes a program.
    ///
    /// In order to make sure that you execute the newest version, `prepare` should be called beforehand.
    pub fn execute(&self, arguments: &[&str]) -> Result<(), Error> {
        if self
            .get_execute_cmd()
            .args(arguments)
            .status()
            .context("child execution")?
            .code()
            == Some(0)
        {
            Ok(())
        } else {
            Err(anyhow!("child non-zero exit"))
        }
    }

    /// Executes the program, passing the given string into standard input.
    ///
    /// In order to make sure that you execute the newest version, `prepare` should be called beforehand.
    pub fn execute_with_stdin(&self, arguments: &[&str], stdin: &str) -> Result<(), Error> {
        let mut child = self
            .get_execute_cmd()
            .args(arguments)
            .stdin(Stdio::piped())
            .spawn()
            .context("child execution")?;
        // .take() will cause the handle to be dropped, closing stdin
        child
            .stdin
            .take()
            .unwrap()
            .write_all(stdin.as_bytes())
            .context("child stdin write")?;
        if child.wait().context("child wait")?.code() == Some(0) {
            Ok(())
        } else {
            Err(anyhow!("child non-zero exit"))
        }
    }

    /// Runs the given `test` and returns the status.
    pub fn test(&self, test: &Test) -> Result<Outcome, Error> {
        test.test(self.get_execute_cmd()).context("test command")
    }
}

impl CompiledProgram {
    /// Compiles this source file if it is newer than the corresponding target file (or the target
    /// does not exist).
    pub fn recompile_if_old(&self, debug: bool) -> Result<(), Error> {
        let should_recompile = match (
            std::fs::metadata(&self.source).and_then(|meta| meta.modified()),
            std::fs::metadata(&self.target).and_then(|meta| meta.modified()),
        ) {
            (Err(err), _) => return Err(anyhow!(err).context("source file metadata")),
            (Ok(_), Err(_)) => true,
            (Ok(src_time), Ok(tgt_time)) => src_time > tgt_time,
        };

        if should_recompile {
            self.recompile(debug)
        } else {
            Ok(())
        }
    }

    /// Compiles this program.
    pub fn recompile(&self, debug: bool) -> Result<(), Error> {
        if Command::new(self.language.compiler())
            .arg(&self.source)
            .arg("-o")
            .arg(&self.target)
            .args(self.language.compiler_arguments(debug))
            .stdin(Stdio::null())
            .status()
            .context("compiler execution")?
            .code()
            == Some(0)
        {
            Ok(())
        } else {
            Err(anyhow!("compilation failed"))
        }
    }
}
