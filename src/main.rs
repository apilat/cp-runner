use anyhow::{anyhow, Context, Error};
use clap::{App, Arg};
use home::home_dir;
use std::path::PathBuf;
mod language;
mod log;
mod program;
mod test;
use log::set_logging_enabled;
use program::Program;
use test::{Status, Test};

fn main() -> Result<(), Error> {
    // TODO Properly handle path instead of converting to string
    let _default_template_location = format!(
        "{}/.config/cp-runner/templates/",
        home_dir()
            .ok_or_else(|| anyhow!("failed to find home directory"))?
            .display()
    );

    let app = {
        let name_arg = Arg::with_name("name").required(true).help("Program name");
        let args_run = [
            name_arg.clone(),
            Arg::with_name("debug")
                .short("d")
                .long("debug")
                .help("Compile with debug options"),
            Arg::with_name("force")
                .short("f")
                .long("force-recompile")
                .help("Force recompilation"),
        ];

        App::new("cp-runner")
            .version("0.1")
            .about("Compiles and tests competitive programming solutions")
            .arg(
                Arg::with_name("verbose")
                    .short("v")
                    .long("verbose")
                    .help("Print more detailed information"),
            )
            .subcommand({
                let run = App::new("run")
                    .about("Run the program, compiling if necessary")
                    .args(&args_run)
                    .arg(
                        Arg::with_name("passthrough")
                            .multiple(true)
                            .help("Extra arguments"),
                    );

                if cfg!(feature = "clipboard-input") {
                    run.arg(
                        Arg::with_name("clipboard")
                            .short("c")
                            .long("clipboard")
                            .help("Pass clipboard contents as standard input"),
                    )
                } else {
                    run
                }
            })
            .subcommand(
                App::new("build")
                    .about("Recompile the program if it has changed")
                    .args(&args_run),
            )
            .subcommand(
                App::new("test")
                    .about("Run tests on the program")
                    .args(&args_run),
            )
            .subcommand(
                App::new("new")
                    .about("Create new program from template")
                    .arg(&name_arg)
                    .arg(
                        Arg::with_name("template")
                            .short("t")
                            .long("template")
                            .required(true)
                            .takes_value(true)
                            .help("Template name"),
                    )
                    .arg(
                        Arg::with_name("location")
                            .short("L")
                            .long("location")
                            .takes_value(true)
                            .default_value(&_default_template_location)
                            .help("Template location"),
                    ),
            )
    };

    let help_string = {
        let mut v = Vec::new();
        app.write_help(&mut v).unwrap();
        String::from_utf8(v).expect("App help is not valid UTF-8")
    };
    let matches = app.get_matches();

    set_logging_enabled(matches.is_present("verbose"));

    match matches.subcommand() {
        ("run", Some(matches)) => {
            let prog = Program::find_by_path(PathBuf::from(matches.value_of_os("name").unwrap()))?;
            let arguments = matches
                .values_of("passthrough")
                .map(Iterator::collect)
                .unwrap_or_else(Vec::new);
            prog.prepare(matches.is_present("debug"), matches.is_present("force"))?;
            if matches.is_present("clipboard") {
                prog.execute_with_stdin(&arguments, &get_clipboard_content()?)
            } else {
                prog.execute(&arguments)
            }
        }

        ("build", Some(matches)) => {
            let prog = Program::find_by_path(PathBuf::from(matches.value_of_os("name").unwrap()))?;
            prog.prepare(matches.is_present("debug"), matches.is_present("force"))
        }

        ("test", Some(matches)) => {
            let path = PathBuf::from(matches.value_of_os("name").unwrap());
            let prog = Program::find_by_path(path.clone())?;
            prog.prepare(matches.is_present("debug"), matches.is_present("force"))?;

            let tests = Test::find_all(path).context("finding tests")?;
            let mut any_failed = false;
            for test in tests.iter() {
                print!("{} ... ", test.name());
                let outcome = prog.test(test)?;
                match outcome.status {
                    Status::Success => println!("Success. ({}ms)", outcome.time.as_millis()),
                    Status::Failure {
                        index,
                        expected,
                        found,
                    } => {
                        println!(
                            "FAILURE! ({}ms) At index {} expected {} but found {}",
                            outcome.time.as_millis(),
                            index,
                            expected,
                            found
                        );
                        any_failed = true;
                    }
                }
            }

            if any_failed {
                Err(anyhow!("some tests failed"))
            } else {
                Ok(())
            }
        }

        ("new", Some(matches)) => {
            Program::create(PathBuf::from(matches.value_of_os("name").unwrap()), {
                let mut buf = PathBuf::from(matches.value_of("location").unwrap());
                buf.push(matches.value_of("template").unwrap());
                buf
            })?;
            Ok(())
        }

        _ => {
            println!("{}", help_string);
            Ok(())
        }
    }
}

#[cfg(feature = "clipboard-input")]
fn get_clipboard_content() -> anyhow::Result<String> {
    // Clipboard errors don't implement Send so re-pack them with anyhow!
    use clipboard::{ClipboardContext, ClipboardProvider};
    let mut provider: ClipboardContext =
        ClipboardProvider::new().map_err(|_| anyhow!("clipboard provider"))?;
    provider
        .get_contents()
        .map_err(|_| anyhow!("clipboard contents"))
}

#[cfg(not(feature = "clipboard-input"))]
fn get_clipboard_content() -> anyhow::Result<String> {
    unreachable!("binary not compiled with clipboard support")
}
