use crate::log;
use anyhow::{anyhow, Context};
use std::cmp::Ordering;
use std::collections::HashSet;
use std::ffi::OsStr;
use std::fmt::{self, Display, Formatter};
use std::fs;
use std::path::{Path, PathBuf};
use std::process::{ChildStdin, ChildStdout};
use std::process::{Command, Stdio};
use std::time::{Duration, Instant};

#[derive(Debug)]
pub struct Test {
    name: String,
    input: Vec<u8>,
    output: Vec<u8>,
}

#[derive(Debug, Clone)]
pub struct Outcome {
    pub(crate) status: Status,
    pub(crate) time: Duration,
}

#[derive(Debug, Clone)]
pub enum Status {
    Success,
    Failure {
        index: usize,
        expected: OutputChar,
        found: OutputChar,
    },
}

#[derive(Debug, Clone, Copy)]
pub struct OutputChar(Option<u8>);

impl Display for OutputChar {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.0 {
            None => "<Nothing>".fmt(f),
            Some(c) if c.is_ascii_graphic() => write!(f, "'{}'", c as char),
            Some(c) => write!(f, "{:#x}", c),
        }
    }
}

impl Test {
    /// Creates a new test by reading the input and output files.
    pub fn new(name: String, input: &Path, output: &Path) -> anyhow::Result<Self> {
        Ok(Self {
            name,
            input: fs::read(input).context("input file read")?,
            output: fs::read(output).context("output file read")?,
        })
    }

    /// Tests the given `program` by feeding it the input file and verifying the output matches
    /// the output file.
    pub fn test(&self, mut program: Command) -> anyhow::Result<Outcome> {
        let start_time = Instant::now();

        let mut child = program
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::null())
            .spawn()?;
        let stdin = child.stdin.take().unwrap();
        let stdout = child.stdout.take().unwrap();

        // TODO Return output text in Outcome
        let output = manage_input(stdin, &self.input, stdout).context("capture output")?;
        let time = start_time.elapsed();

        let first_wrong = Iterator::zip(self.output.iter(), output.iter())
            .enumerate()
            .find(|(_, (exp, found))| exp != found);

        let status = if let Some((i, (&exp, &found))) = first_wrong {
            Status::Failure {
                index: i,
                expected: OutputChar(Some(exp)),
                found: OutputChar(Some(found)),
            }
        } else {
            match self.output.len().cmp(&output.len()) {
                Ordering::Equal => Status::Success,
                Ordering::Less => Status::Failure {
                    index: self.output.len(),
                    expected: OutputChar(None),
                    found: OutputChar(Some(output[self.output.len()])),
                },
                Ordering::Greater => Status::Failure {
                    index: output.len(),
                    expected: OutputChar(Some(self.output[output.len()])),
                    found: OutputChar(None),
                },
            }
        };

        Ok(Outcome { status, time })
    }

    /// Attempts to find tests for the program named `path`.
    ///
    /// # Location
    /// The only directories which are searched are `.`, `test/` and `tests/` (relative to `path`).
    /// The test must have a prefix of `test` or the filename of `path` (excluding the extension).
    /// The files must come in pairs with suffixes `.in` and `.out`.
    pub fn find_all(mut path: PathBuf) -> anyhow::Result<Vec<Test>> {
        path.set_extension("");
        let original_name = match path.file_name().and_then(OsStr::to_str) {
            Some(x) => x.to_owned(),
            None => return Err(anyhow!("no filename")),
        };
        path.pop();

        let mut found_tests = Vec::new();

        for dir in ["", "test", "tests"].iter() {
            let mut found_half = HashSet::new();
            path.push(dir);
            // Ignore read_dir errors since we're essentially guessing possible directories which
            // might not exist etc
            if let Ok(dir_iter) = path.read_dir() {
                for file in dir_iter {
                    let file = file.context("directory listing")?;
                    let name = match file.file_name().into_string() {
                        Ok(x) => x,
                        Err(_) => continue,
                    };

                    if file.metadata().context("metadata")?.is_file()
                        && (name.starts_with("test") || name.starts_with(&original_name))
                    {
                        if let Some(proper_name) = name
                            .strip_suffix(".in")
                            .or_else(|| name.strip_suffix(".out"))
                        {
                            // This can likely be done without all of the reallocations but it is
                            // probably not worth the trouble.
                            if found_half.contains(proper_name) {
                                log!("Found test {}", proper_name);
                                let mut in_name = path.join(proper_name);
                                in_name.set_extension("in");
                                found_tests.push(Test::new(
                                    proper_name.to_owned(),
                                    &in_name,
                                    &in_name.with_extension("out"),
                                )?);
                            } else {
                                found_half.insert(proper_name.to_owned());
                            }
                        }
                    }
                }
            }
            path.pop();
        }

        Ok(found_tests)
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

// TODO Add timeout and output size limit
#[cfg(unix)]
fn manage_input(
    stdin: ChildStdin,
    input: &[u8],
    mut stdout: ChildStdout,
) -> anyhow::Result<Vec<u8>> {
    use nix::{
        fcntl::{fcntl, FcntlArg, OFlag},
        poll::{poll, PollFd, PollFlags},
    };
    use std::{
        io::{Read, Write},
        os::unix::prelude::AsRawFd,
    };

    {
        let flags = fcntl(stdin.as_raw_fd(), FcntlArg::F_GETFL).context("fcntl get stdin flags")?;
        fcntl(
            stdin.as_raw_fd(),
            FcntlArg::F_SETFL(OFlag::from_bits(flags).unwrap() | OFlag::O_NONBLOCK),
        )
        .context("fcntl set stdin non-blocking")?;
    }

    let mut fds: [PollFd; 2] = [
        PollFd::new(stdout.as_raw_fd(), PollFlags::POLLIN),
        PollFd::new(stdin.as_raw_fd(), PollFlags::POLLOUT),
    ];

    // Store stdin in an option to allow dropping (closing the pipe) when we are done writing.
    let mut stdin = Some(stdin);
    let mut read_buf = [0; 128 * 1024];
    let mut written = 0;
    let mut output = Vec::new();

    'poll: loop {
        if stdin.is_some() {
            poll(&mut fds, -1).context("poll")?;
        } else {
            poll(&mut fds[..1], -1).context("poll")?;
        }

        // TODO Don't error on ErrorKind::Interrupted
        if let Some(ref mut stdin_ref) = stdin {
            if let Some(flags) = fds[1].revents() {
                if flags.intersects(PollFlags::POLLERR | PollFlags::POLLNVAL) {
                    return Err(anyhow!("stdin poll error"));
                } else if flags.intersects(PollFlags::POLLOUT) {
                    let x = stdin_ref.write(&input[written..]).context("stdin write")?;
                    written += x;
                    if x == 0 || written == input.len() {
                        // Close stdin and make sure it is no longer polled
                        drop(stdin.take());
                    }
                }
            }
        }

        if let Some(flags) = fds[0].revents() {
            if flags.intersects(PollFlags::POLLERR | PollFlags::POLLNVAL) {
                return Err(anyhow!("stdout poll error"));
            } else if flags.intersects(PollFlags::POLLIN | PollFlags::POLLHUP) {
                // Try to read on hangup in case there is still data left
                let x = stdout.read(&mut read_buf).context("stdout write")?;
                if x == 0 {
                    break 'poll;
                }
                output.extend_from_slice(&read_buf[..x]);
            }
        }
    }

    Ok(output)
}
