use std::sync::atomic::{AtomicBool, Ordering};

static LOGGER_ENABLED: AtomicBool = AtomicBool::new(false);

pub fn set_logging_enabled(on: bool) {
    LOGGER_ENABLED.store(on, Ordering::Release)
}

pub fn is_logging_enabled() -> bool {
    LOGGER_ENABLED.load(Ordering::Acquire)
}

#[macro_export]
macro_rules! log {
    ($($e:expr),+) => {
        if $crate::log::is_logging_enabled() {
            eprint!("[D] ");
            eprintln!( $($e),+ );
        }
    }
}
