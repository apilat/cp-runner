use self::{CompiledLanguage::*, InterpretedLanguage::*, Language::*};
use std::fmt::{self, Display, Formatter};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Language {
    Compiled(CompiledLanguage),
    Interpreted(InterpretedLanguage),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum CompiledLanguage {
    Cpp,
    Rust,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum InterpretedLanguage {
    Python,
}

impl Language {
    /// Returns an iterator over all available language types.
    pub fn iter() -> impl Iterator<Item = Language> {
        [Compiled(Cpp), Compiled(Rust), Interpreted(Python)]
            .iter()
            .copied()
    }

    /// Returns the common file extension associated with this language. The extension does not
    /// include the dot.
    pub fn extension(self) -> &'static str {
        match self {
            Compiled(Cpp) => "cpp",
            Compiled(Rust) => "rs",
            Interpreted(Python) => "py",
        }
    }

    /// Finds the language with a corresponding extension, if one exists.
    pub fn by_extension(ext: &str) -> Option<Self> {
        Self::iter().find(|lang| lang.extension() == ext)
    }
}

impl CompiledLanguage {
    /// Returns the name of the compiler executable.
    pub fn compiler(self) -> &'static str {
        match self {
            Cpp => "g++",
            Rust => "rustc",
        }
    }

    /// Returns the extra arguments that should be passed to the compiler.
    ///
    /// The compiler should be called with `{compiler()} {source} -o {target} {.compiler_arguments()}`
    pub fn compiler_arguments(self, debug: bool) -> &'static [&'static str] {
        if debug {
            match self {
                Cpp => &[
                    "-Wall",
                    "-Wextra",
                    "-lm",
                    "-ggdb",
                    "-fsanitize=address",
                    "-fsanitize=undefined",
                ],
                Rust => &["-g"],
            }
        } else {
            match self {
                Cpp => &["-Wall", "-Wextra", "-O2", "-static", "-lm"],
                Rust => &["-O"],
            }
        }
    }
}

impl InterpretedLanguage {
    /// Returns the name of the interpreter executable.
    pub fn interpreter(self) -> &'static str {
        match self {
            Python => "python3",
        }
    }
}

impl Display for Language {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Compiled(Cpp) => "C++".fmt(f),
            Compiled(Rust) => "Rust".fmt(f),
            Interpreted(Python) => "Python3".fmt(f),
        }
    }
}
